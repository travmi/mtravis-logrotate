# == Class: logrotate
#
# Adapted from http://projects.puppetlabs.com/projects/1/wiki/Logrotate_Patterns
#
# This manages the installation of logrotate.
#
# === Parameters
#
# There are no paramters for this class.
#
# === Examples
#
#  include logrotate
#
# === Authors
#
# Author Name <mtravis@webcourseworks.com>
#
# === Copyright
#
# Copyright 2014 Mike Travis, unless otherwise noted.
#
class logrotate {
  package { 'logrotate':
    ensure => present,
  }

  file { '/etc/logrotate.d':
    ensure  => directory,
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    require => Package['logrotate'],
  }
}
