# logrotate

#### Table of Contents

1. [Overview](#overview)
2. [Setup - The basics of getting started with logrotate](#setup)
    * [What logrotate affects](#what-logrotate-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with logrotate](#beginning-with-logrotate)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Overview

This module manages logrotate and it's config files.

## Setup

### What logrotate affects

* logrotate directory.
* logrotate config files.

### Beginning with logrotate

The very basic steps needed for a user to get the module up and running.

```puppet
include logrotate
```

Configuring a logrotate config file.

```puppet
logrotate::file { 'awesant':
  log        => '/var/log/awesant/*.log',
  options    => [ 'daily',
                  'notifemty',
                  'rotate 7',
                  'compress',
                  'delaycompress' ],
  postrotate => [ '' ],         
}
```

##Usage

All interaction with the logrotate module can do be done through the main logrotate class.

## Reference

###Classes

####Public Classes

* logrotate: Main class, ensures logrotate is installed.
* logrotate::file: Defined type for configuration files.

##Limitations

This module has been built on and tested against Puppet Enterprise 3.3 and higher.

The module has been tested on:

* CentOS 6.5+

Testing on other platforms has not been and is currently not supported. 

##Development

###Contributors

Mike Travis - <mtravis@webcourseworks.com>


